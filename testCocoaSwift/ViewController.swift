//
//  ViewController.swift
//  testCocoaSwift
//
//  Created by Milos Visnjic on 9/18/19.
//  Copyright © 2019 Milos Visnjic. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBOutlet weak var imgButton: NSButton!
    
    @IBAction func myButton(_ sender: NSButton) {
    //mt btn
        imgButton.isHidden = !imgButton.isHidden;
    }
    
    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }


}

