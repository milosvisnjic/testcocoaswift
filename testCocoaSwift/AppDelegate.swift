//
//  AppDelegate.swift
//  testCocoaSwift
//
//  Created by Milos Visnjic on 9/18/19.
//  Copyright © 2019 Milos Visnjic. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

